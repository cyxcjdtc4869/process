%% load data
load Data_USEconModel
logsq = csvread('logsq.csv');
logvo = csvread('logvo.csv');
logrv = csvread('logrv.csv');
sq = csvread('FTSEGT.csv');
rv = csvread('RV.csv');
vo = csvread('VO.csv');
gdp = DataTable.GDP;
m1 = DataTable.M1SL;
tb3 = DataTable.TB3MS;
% Y = [gdp,m1,tb3];
% Y = [logsq,logvo,logrv];
Y = [rv,sq,vo];

%% date
n = 731950;
m = 1;
date = zeros((731950-735603)/7,1);
while  n<= 736513
   date(m,:) = n; 
   n = n+7;
   m = m+1;
end 


%% plot data
% figure
subplot(3,1,1)
plot(date,Y(:,1),'r');
title('Realized Volatility')
datetick('x')
grid on
subplot(3,1,2);
plot(date,Y(:,2),'b');
title('Search Queries')
datetick('x')
grid on
subplot(3,1,3);
plot(date, Y(:,3), 'k')
title('Trading Volume')
datetick('x')
grid on
hold off

%% log return
Y = [diff(log(Y(:,1:2))), Y(2:end,3)]; % Transformed data
X = dates(2:end);

% figure
% subplot(3,1,1)
% plot(X,Y(:,1),'r');
% title('GDP')
% datetick('x')
% grid on
% subplot(3,1,2);
% plot(X,Y(:,2),'b');
% title('M1')
% datetick('x')
% grid on
% subplot(3,1,3);
% plot(X, Y(:,3),'k'),
% title('3-mo T-bill')
% datetick('x')
% grid on

%% plot in one figure
% Y(:,1:2) = 100*Y(:,1:2);
% figure
% plot(X,Y(:,1),'r');
% hold on
% plot(X,Y(:,2),'b');
% datetick('x')
% grid on
% plot(X,Y(:,3),'k');
% legend('GDP','M1','3-mo T-bill');
% hold off

%% fit model

% same length
% dGDP = 100*diff(log(gdp(49:end)));
% dM1 = 100*diff(log(m1(49:end)));
% dT3 = diff(tb3(49:end));
% Y = [dGDP dM1 dT3];
Y = [logsq,logvo,logrv];
% 4 models
dt = logical(eye(3));
VAR2diag = vgxset('ARsolve',repmat({dt},2,1),...
    'asolve',true(3,1),'Series',{'logrv','logsq','logvl'});
VAR2full = vgxset(VAR2diag,'ARsolve',[]);
VAR4diag = vgxset(VAR2diag,'nAR',4,'ARsolve',repmat({dt},4,1));
VAR4full = vgxset(VAR2full,'nAR',4);

YPre = Y(1:4,:);
T = ceil(.95*size(Y,1));
YEst = Y(5:T,:);
YF = Y((T+1):end,:);



% fit models
[EstSpec1,EstStdErrors1,logL1,W1] = ...
    vgxvarx(VAR2diag,YEst,[],YPre,'CovarType','Diagonal');
[EstSpec2,EstStdErrors2,logL2,W2] = ...
    vgxvarx(VAR2full,YEst,[],YPre);
[EstSpec3,EstStdErrors3,logL3,W3] = ...
    vgxvarx(VAR4diag,YEst,[],YPre,'CovarType','Diagonal');
[EstSpec4,EstStdErrors4,logL4,W4] = ...
    vgxvarx(VAR4full,YEst,[],YPre);
TF = size(YF,1);


% check the model adequacy
% [isStable1,isInvertible1] = vgxqual(EstSpec1);
% [isStable2,isInvertible2] = vgxqual(EstSpec2);
% [isStable3,isInvertible3] = vgxqual(EstSpec3);
% [isStable4,isInvertible4] = vgxqual(EstSpec4);
% [isStable1,isStable2,isStable3,isStable4]


% compare
[FY1,FYCov1] = vgxpred(EstSpec1,TF,[],YEst);
[FY2,FYCov2] = vgxpred(EstSpec2,TF,[],YEst);
[FY3,FYCov3] = vgxpred(EstSpec3,TF,[],YEst);
[FY4,FYCov4] = vgxpred(EstSpec4,TF,[],YEst);

% sse
Error1 = YF - FY1;
Error2 = YF - FY2;
Error3 = YF - FY3;
Error4 = YF - FY4;

SSerror1 = Error1(:)' * Error1(:);
SSerror2 = Error2(:)' * Error2(:);
SSerror3 = Error3(:)' * Error3(:);
SSerror4 = Error4(:)' * Error4(:);
% figure
% bar([SSerror1 SSerror2 SSerror3 SSerror4],.5)
% ylabel('Sum of squared errors')
% set(gca,'XTickLabel',...
%     {'AR2 diag' 'AR2 full' 'AR4 diag' 'AR4 full'})
% title('Sum of Squared Forecast Errors')

figure
vgxplot(EstSpec2,YEst,FY2,FYCov2)
% mse
% MSerror1 = SSerror1/length(Error1);
MSerror1 = (sum(Error1.^2))/size(Error1,1);
MSerror2 = (sum(Error2.^2))/size(Error2,1);
MSerror3 = (sum(Error3.^2))/size(Error3,1);
MSerror4 = (sum(Error4.^2))/size(Error4,1);

vgxdisp(EstSpec2)

%% forcast observation
% YFirst = [gdp,m1,tb3];
% [YPred,YCov] = vgxpred(EstSpec2,10,[],YF);
% YFirst = [logsq,logvo,logrv];
% YFirst = YFirst(49:end,:);           % Remove NaNs
% dates = dates(49:end);
% EndPt = YFirst(end,:);
% EndPt(1:2) = log(EndPt(1:2));
% YPred(:,1:2) = YPred(:,1:2)/100;     % Rescale percentage
% YPred = [EndPt; YPred];              % Prepare for cumsum
% YPred(:,1:3) = cumsum(YPred(:,1:3));
% YPred(:,1:2) = exp(YPred(:,1:2));
% lastime = dates(end);
% timess = lastime:91:lastime+910;     % Insert forecast horizon
% 
% figure
% subplot(3,1,1)
% plot(timess,YPred(:,1),':r')
% hold on
% plot(dates,YFirst(:,1),'k')
% datetick('x')
% grid on
% title('GDP')
% subplot(3,1,2);
% plot(timess,YPred(:,2),':r')
% hold on
% plot(dates,YFirst(:,2),'k')
% datetick('x')
% grid on
% title('M1')
% subplot(3,1,3);
% plot(timess,YPred(:,3),':r')
% hold on
% plot(dates,YFirst(:,3),'k')
% datetick('x')
% grid on
% title('3-mo T-bill')
% hold off