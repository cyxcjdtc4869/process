%% load data
close all; 
clear all;
FTSEGT = csvread('FTSE100_GLB.csv');
FTSECP = csvread('FTSE100CP.csv');
FTSEVL = csvread('FTSE100VL.csv');


%% time scale
n = 731950;
m = 1;
date = zeros((731950-735603)/7,1);
while  n<= 736513
   date(m,:) = n; 
   n = n+7;
   m = m+1;
end 

%% time lag
weekly = zeros(653,1);
n = 1;
for i = 1:653
    part = FTSEVL(n:n+4,1);
    subpart = length(find(part==0));
    sum = FTSEVL(n,1)+FTSEVL(n+1,1)+FTSEVL(n+2,1)+FTSEVL(n+3,1)+FTSEVL(n+4,1);
     weekly(i,1) = sum/(5-subpart);
    n = n+5;
end
a = weekly/10^9;
% csvwrite('volume.csv',a)
VO = a(2:653,:);
%% log return
lreturn = zeros(653,1);
sigma = zeros(1,1);
dataset = FTSECP;
lreturn(:,1) = diff(log(dataset(1:5:end))); 
% csvwrite('lretrn.csv',lreturn)

%% Realised Volatility
lr = lgrt(FTSECP);
RV = anderson(lr);

%% plot figure
figure(1)
plot(date,lreturn(2:653,1)),datetick;
title('Log-return of the FTSE closing price');
% figure(2)
% plot(date,RV,'r'),datetick;
% title('Realised volatility of the FTSE-100')
% xlabel('Years')
% ylabel('Realised volatility');
% figure(3)
% plot(date,weekly(2:653,:)),datetick;
% figure(3)
% plot(date,FTSEGT),datetick;
% title('Search queries fot the name of FTSE-100')
% xlabel('Years')
% ylabel('Search queries');
% figure(4)
% hold on
% plot(date,VO),datetick;

% ske = skewness(RV);kur = kurtosis(RV);
% legend('skewness = '+ske, 'kurtosis = '+kur);
% set(h(1),'Visible','Off');
%% correlation
% co = corrcoef(FTSEGT,VO);
% %  co1 = corrcoef(FTSEGT,RV);
%  logrv = log(RV);
% %  co2 = corrcoef(FTSEGT,logrv);
%  logsq = log(FTSEGT);
%  logvo = log(VO);
%% Distribution
figure(5)
subplot(2,1,1)
h  = histfit(RV,60);
grid on;
title('Distribution of realized bolatility');
subplot(2,1,2)
h  = histfit(logrv,60);
grid on;
title('Distribution of log of realized bolatility');
%% Save the data
csvwrite('RV.csv',RV)
csvwrite('logrv.csv',logrv)
csvwrite('FTSEGT.csv',FTSEGT)
csvwrite('logsq.csv',logsq)
csvwrite('VO.csv',VO)
csvwrite('logvo.csv',logvo)


