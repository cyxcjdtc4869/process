%% load data
RV = csvread('RV.csv');
logrv = csvread('logrv.csv');
FTSEGT = csvread('FTSEGT.csv');
logsq = csvread('logsq.csv');
VO = csvread('VO.csv');
logvo = csvread('logvo.csv');

%% build model

% x1 = [logrv;zeros(2,1)];
% x2 = [logsq;zeros(152,1)];
% input = x1(1:198);
pre = zeros(32,1);
x = logrv;
input1 = [x(1:620);pre];
input = iddata(input1);

% output = ar(input,4);
% p = predict(input(1:652),output,52);
%% predict
table = zeros(1,30);
for i = 1:30
output = ar(input,i);
p = predict(input(1:652),output,32);
xp1 = p.OutputData;
Error = x(621:652,1)-xp1(621:652,1);
MSerror = (sum(Error.^2))/size(Error,1);
table (1,i) = MSerror;
end

% figure (1)
% plot(xp1,'black');
% hold on
% figure (2)
% plot(logrv(1:201),'g');
% title('Predict result');
% legend('predict','input');

