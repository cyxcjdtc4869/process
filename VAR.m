%% load data
RV = csvread('RV.csv');
logrv = csvread('logrv.csv');
SQ = csvread('FTSEGT.csv');
logsq = csvread('logsq.csv');
VO = csvread('VO.csv');
logvo = csvread('logvo.csv');

%% Create model
Spec = vgxset('n',2,'nAR',4,'Constant',true)

%% Fit data
Y =[RV,SQ];
[EstSpec,EstStdErrors,logL,W] = vgxvarx(Spec,Y);
vgxdisp(EstSpec,EstStdErrors)
%% plot
% EstW = vgxinfer(EstSpec, Y, [], 2, W0);

% subplot(2,1,1);
% plot([ W(:,1), EstW(:,1) ]);
% subplot(2,1,2);
% plot([ W(:,2), EstW(:,2) ]);
% legend('VARMA(2, 2)', 'VAR(2)');

%% Forcast
FT = 10;
FY = vgxpred(Spec,FT);
% [FY,FYCov] = vgxpred(Spec,FT,[],Y,W);
% vgxplot(Spec, [], FY, FYCov);

