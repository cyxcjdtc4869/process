%% load data
RV = csvread('RV.csv');
logrv = csvread('logrv.csv');
FTSEGT = csvread('FTSEGT.csv');
logsq = csvread('logsq.csv');
VO = csvread('VO.csv');
logvo = csvread('logvo.csv');
%% granger
[q v]=granger_cause(FTSEGT,logsq,0.05,2);