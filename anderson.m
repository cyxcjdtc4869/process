function result = anderson(x)
temp = zeros(652,1);
n = 1;
for i = 1:652
    part = x(n:n+4,1);
    temp(i,1) = sqrt(sum((part.^2)));
    n = n+5;
end
result = temp;
end