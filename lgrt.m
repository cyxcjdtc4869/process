function result = lgrt(x)
a = zeros(length(x)-1,1);
xx = log(x);
for i = 1:length(x)-1
a(i,1)= xx(i+1,1)-xx(i,1);
end
result = a;
end