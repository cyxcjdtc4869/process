clc;clear;close all;
warning off all;

dataset = load('Dataset_70.dat');
Dataset =dataset(:,4:13);
Dates = dataset(:,3:-1:1);
Dates(:,4:6) = 0;
Dates = datenum(Dates);

lreturns = zeros(4025,10);
expectation = zeros(10,1);
sigma = zeros(10,1);
for i = 1:10
    data = Dataset(:,i);
    lreturns(:,i) = diff(log(data));    
    expectation(i) = mean(lreturns(:,i));
    sigma(i) = std(lreturns(:,i));
    %set(gca,'xlim',[0,2013]);
end
interval = [5 10 100 250];
for i = 1:10
    data = Dataset(:,i);
    lreturns(:,i) = diff(log(data(1:249:end)));    
    expectation250(i) = mean(lreturns(:,i));
    sigma250(i) = std(lreturns(:,i));
end

a5=(-sigma5+ sigma'*5^0.5)./sigma5;
a10=(-sigma10+ sigma'*10^0.5)./sigma10;
a100=(-sigma100+ sigma'*100^0.5)./sigma100
a250=(-sigma250+ sigma'*250^0.5)./sigma250

hold on
plot(a5,'o-');plot(a10,'o-');plot(a100,'o-');plot(a250,'o-');
legend('5-day','10-day','100-day','250-day')
title('Relative error between original volatility and scaling volatility','fontsize',20)
xlabel('Stock')
ylabel('Relative error')

data = Dataset(:,10);
for i = 1:2012
    lreturns = diff(log(data(1:i:end)));    
    sigma10(i) = std(lreturns);
end
figure
hold on;
plot(sigma1);plot(sigma2);plot(sigma3);plot(sigma4);plot(sigma5);
plot(sigma6);plot(sigma7);plot(sigma8);plot(sigma9);plot(sigma10);
legend('A','B','C','D','E','F','G','H','I','J')
title('Variance vs Time','fontsize',20)
xlabel('Time','fontsize',20)
ylabel('Standard deviation','fontsize',20)
set(gca,'xlim',[0 2012]);

%{
npaths = 1; % number of paths
T = 100000; % time horizon
nsteps = 4026; % number of time steps
dt = T/nsteps; % time step
t = 0:dt:T; % observation times
mu = 0; sigma = 100;
dX = sigma*sqrt(dt)*randn(nsteps,npaths);
X = [zeros(1,npaths); cumsum(dX)];
for i = 1:4026
    XX = X(1:i);    
    sigma11(i) = std(XX);
end
figure
plot(sigma11)
%}

% stationary
clc
lags = (0:1500)';
 [h,pValue,stats] = kpsstest(Dataset(:,10),'Lags',lags,'Trend',true);
 results = [lags h  pValue stats]
% [~,pValue,stats] = kpsstest(ret(:,1),'Lags',lags,'Trend',true);
 [h,pValue,stats] = kpsstest(lreturns(:,10),'Lags',lags,'Trend',true);
results = [lags h  pValue stats]

% p = 0.1 or 0.01, nonstationary
% 上次作业移动窗口

[h,p,ks2stat] = kstest2(nsample,Portreturn,'Alpha',0.1)


% 变成点加趋势线
close all
for i = 1:10
    subplot(2,5,i)
    [a1,lags1] = autocorr(abs(lreturns(:,i)),1000);
    plot(log(lags1),log(a1),'.r')
    hold on;
    % plot([0 10],[0 0],'-k')
    % axis([0 10 -.25 0.25])
    xlabel('log-lags(days)','fontsize',14)
    ylabel('log-autocorrelation','fontsize',14)
    title('Autocorrelation of returns','fontsize',14)
    set(gca,'fontsize',14)
    legend('|lreturns|')
end

close all
for i = 1:10
    subplot(2,5,i)
    [a1,lags1] = autocorr(abs(lreturns(:,i)),10);
    plot(lags1,a1,'.r')
    hold on;
    plot([0 10],[0 0],'-k')
    axis([0 10 -.25 0.25])
    xlabel('lags (days)','fontsize',14)
    ylabel('autocorrelation','fontsize',14)
    title('Autocorrelation of returns','fontsize',14)
    set(gca,'fontsize',14)
    legend('|lreturns|')
end
close all
for i = 1:10
    subplot(2,5,i)
    [a1,lags1] = autocorr(lreturns(:,i),1000);
    plot(lags1,a1,'-r')
    hold on
    [a2,lags2] = autocorr(abs(lreturns(:,i)),1000);
    plot(lags2,a2,'-m')
    [a3,lags3] = autocorr(lreturns(:,i).^2,1000);
    plot(lags3,a3,'-b')
    plot([0 1000],[0 0],'-k')
    axis([0 1000 -.25 0.25])
    xlabel('lags(days)','fontsize',14)
    ylabel('autocorrelation','fontsize',14)
    title('Autocorrelation of returns','fontsize',14)
    set(gca,'fontsize',14)
    legend({'returns','|returns|','returns^2'})
end




for i = 1:10
    fracdata(:,i) = Dataset(:,i)./max(Dataset(:,i));
end
x1 = linspace(0,1,4026);
%{
p=0:0.01:1;
q=0:0.01:1;
[x,y] = meshgrid(p,q);
plot(x,y,'c')
hold on
plot(x',y','c')
%}
t = [0.001 0.005 0.01 0.05 0.1];
for p = 1:10
    data_tmp = fracdata(floor(linspace(2,4026,1001)),p);
    subplot(5,2,p)
    x3 = 1:4026;
    plot(x3./4026,fracdata(:,p),'r','linewidth',1)
    hold on;
    legend('Standardized log-return')
    for i = 1:1000
        h = data_tmp(i+1)-data_tmp(i);
        b(i) = ceil(abs(h)/0.001);
        rectangle('position',[0.001*(i-1),data_tmp(i)+judge(h),0.001,abs(h)],'facecolor','k')
    end
    hold off;
    x2 = 0:0.01:1;
    frac_a(p)=log(sum(b))/log(1/0.001)
end
suptitle('Fractal properties of Standardized log-return with G=0.001','fontsize',26)

for i = 1:10
    fracdata(:,i) = Dataset(:,i)./max(Dataset(:,i));
end
for p = 1:10
    data_tmp = fracdata(floor(linspace(2,4026,201)),p);
    for i = 1:200
        h = data_tmp(i+1)-data_tmp(i);
        b(i) = ceil(abs(h)/0.005);
    end
    frac_a(p)=log(sum(b))/log(1/0.005)
end

gg = [0.001 0.002 0.005 0.01 0.02 0.05 0.1 0.2 0.5];
df(1,:)=[1.4061    1.4239    1.3828    1.4020    1.3626    1.4284    1.4335    1.3750    1.4242    1.4220];
df(2,:)=[1.3889    1.4207    1.3653    1.3870    1.3409    1.4158    1.4184    1.3685    1.4140    1.4166];
df(3,:)=[1.3673    1.4261    1.3379    1.3619    1.3075    1.4100    1.4123    1.3508    1.3949    1.4133];
df(4,:)=[1.3546    1.4287    1.3227    1.3374    1.2811    1.4004    1.4127    1.3202    1.3920    1.3979];
df(5,:)=[1.3202    1.4282    1.2739    1.3274    1.2501    1.3890    1.3856    1.3128    1.3656    1.3776];
df(6,:)=[1.2632    1.4229    1.2991    1.2555    1.2143    1.3722    1.3496    1.3125    1.3777    1.3316];
df(7,:)=[1.2788    1.4624    1.2553    1.2304    1.1461    1.3424    1.2041    1.2788    1.3424    1.2041];
df(8,:)=[1.1133    1.4307    1.2091    1.2091    1.2920    1.2920    1.2920    1.2091    1.1133    1.2091];
df(9,:)=[2.0000    1.0000    1.0000    1.0000    1.5850    1.0000    1.5850    1.5850    1.5850    1.0000];

for i =1:10
    hold on;
    plot(df(1:8,i),gg(1:8))
end
legend('A','B','C','D','E','F','G','H','I','J')
xlabel('Fractal Dimension')
ylabel('G')
title('G vs Fractal Dimension ')


for i =1:10
    hur(i) = genhurst(Dataset(:,i),2);
end
hur+df(1,:)

a_scaling = 1./(2-df(1,:));
for i=1:10
    a_power(i)=plfit(lreturns(:,i),'nosmall');
end






scaling of return distribution
interval = [1 2 3 4 5 10 20 30 40 50 60 70 80 90 100 150 200];
for i = 1:10
    j = 1:17;
    lreturns_interval{i} = arrayfun(@(z) diff(log(Dataset(1:interval(z):end,i))),j,'uniform',false);
end
for i = 6:17
    [a,b] = hist(lreturns_interval{1}{i},[-1 -0.8 -0.6 -0.4 -0.2 0 0.2 0.4 0.6 0.8 1]);
    plot(b,a,'o-');
    hold on;
end

m = 20;
ret = zeros(4025,10);
for i = 1:10
    data = Dataset(:,i);
    ret(:,i) = diff(data);
    % Y1 = cumsum(ret(:,i),1) ;
    % Y2 = cumsum(ret(:,i).^2,1);
    % volatility = sqrt((Y2((m+1):end)-Y2(1:(end-m)))/m-((Y1((m+1):end)-Y1(1:(end-m)))/m).^2) ;
    % subplot(5,2,i);
    % plot(Dates(22:end),volatility,'Color',[0 0 0],'LineWidth',2),datetick
    % title([Title{i}])
    % set(gca,'xlim',[1995 2005]);
    % ylabel('volatility','fontsize',14)
end
pri = cumsum(ret); % an artificial trading only price
i = 1:10;
h = arrayfun(@(z) genhurst(pri(:,z),2),i);
interval = [1 5 10 20 30 40 50 60 90];
for i = 1:10
    j = 1:9;
    lreturns_interval{i} = arrayfun(@(z) diff(log(Dataset(1:interval(z):end,i))),j,'uniform',false);
end
bins=zeros(1,15);
bins1=zeros(1,15);
freqs=zeros(1,15);
for i = 1:10
    for j = 1:9
        [f,b]=hist(lreturns_interval{i}{j},15);
        bins(j,:) = b'/interval(j);
        bins1(j,:) = b'/interval(j)^h(i);
        freqs(j,:) = f'/max(f);
    end
    for j = 1:9
        semilogy(bins(j,:),freqs(j,:),'-+')
        hold on
        legend(num2str(interval(j)))
    end
    for j = 1:9
        semilogy(bins1,freqs,'-+')
        hold on
        legend(num2str(interval(j)))
    end
end




for q = 1:0.1:3
    for interval = 1:19
        lreturns_interval = diff(log(Dataset(1:interval:end,2)));
        %{
dV = diff(lreturns_interval)-
lreturns_interval(1:end-1);
        VV = S(((tt+1):tt:(L+tt))-tt)';
        N = length(dV)+1;
        X = 1:N;
        Y = VV;
        mx = sum(X)/N;
        SSxx = sum(X.^2) - N*mx^2;
        my   = sum(Y)/N;
        SSxy = sum(X.*Y) - N*mx*my;
        cc(1) = SSxy/SSxx;
        cc(2) = my - cc(1)*mx;
        ddVd  = dV - cc(1);
        VVVd  = VV - cc(1).*(1:N) - cc(2);
        %}
        % kq(interval) = mean(abs((diff(lreturns_interval)-lreturns_interval(1:end-1))).^q)/mean(abs(lreturns_interval).*q);
        kq(interval) = mean(abs(diff(lreturns_interval)).^q)/mean(abs(lreturns_interval).*q);
    end
    hold on;
    if q == 1 || q == 2 || q == 3
        plot(log10(1:19),log10(kq),'-ok')
    else
        plot(log10(1:19),log10(kq),'-or')
    end
    hold off
end
q = 0:0.2:5;
for j =1:10
    for i = 1:26
        h(i) = genhurst(Dataset(:,j),q(i));
    end
    subplot(2,5,j)
    plot(q,q.*h,'or-')
    xlabel('q');
    ylabel('qH(q)');

    xlim([0 5]);
    title(['Stock ',num2str(j)],'fontsize',14);
end

plot(h,'or')  
hold on
plot([0 10.5],[0.5 0.5],'-k')
axis([0 10.5 0 1])
xlabel('Stock','fontsize',16)
ylabel('Hurst exponent H(2)','fontsize',16)
title('Are deviations from Brownian motion meaningful?','fontsize',20)


parcorr(lreturns(:,1))

[coeff, errors, LLF, innovations, sigma, summary] = garchfit(lreturns(:,1))

% model = garch('Constant',0.0001,'GARCH',0.75,...
%                                         'ARCH',0.01,'Distribution',struct('Name','t','DoF',8));
model = garch(1,1);
model.Distribution = 't';
EstD1  = estimate(model,lreturns(:,1))
Kappa = EstD1.Constant;
Alpha = cell2mat( EstD1.GARCH );
Beta = cell2mat( EstD1.ARCH );

%{
model = garch(1,1);
model.Distribution = 't';
EstMdl = estimate(model, lreturns(:,1));
V = infer(EstMdl,lreturns(:,1)); %V = conditional variances, also n x 1
xvol = sqrt(V);  %(conditional standard deviations = squareroot of conditional variances)
%}
Mdl = garch('Constant',Kappa,'GARCH',Alpha,'ARCH',Beta);
rng default; % For reproducibility
[v,y] = simulate(Mdl,1000);

vF1 = forecast(Mdl,1000,'Y0',y);

figure
plot(v,'Color',[.7,.7,.7])
hold on
plot(1001:2000,vF1,'r','LineWidth',2);
% plot(101:130,vF2,':','LineWidth',2);
title('Forecasted Conditional Variances','fontsize',20)
legend('Observed','Forecasts with Presamples',...
		'Forecasts without Presamples','Location','NorthEast')
hold off



inter = [1:9 10 15 20:10:100  ];
for i = 1:25
    h(i,10) = genhurst(Dataset(1:inter(i):end,10),2);
end

for i = 1:10
    subplot(2,5,i)
    hold on
    plot(inter,h(:,i),'or')
    plot([0 100], [0.5 0.5],'k','linewidth',2)
    xlabel('Time horizon (Days)')
    ylabel('H(2)')
    set(gca,'ylim',[0 1])
end
suptitle('Are deviations from Brownian motion meaningful?')











%{
%% 娱乐
npaths = 2000; % number of paths
T = 100000; % time horizon
nsteps = 200; % number of time steps
dt = T/nsteps; % time step
t = 0:dt:T; % observation times
mu = 0; sigma = 1;
dX = sigma*sqrt(dt)*randn(nsteps,npaths);
X = [zeros(1,npaths); cumsum(dX)];
aa = zeros(1,11);
for i = 1:200
    [a,b]=hist(X( i,:),[-2000 -1500 -1000 -500 -250 0 250 500 1000 1500 2000]);
    plot3(b,aa,a,'o-k')
    hold on
    plot3(aa,b,a,'o-k')
end
title('Frequencies of Random Walk at Various t in 3-D','fontsize',22)
set(gca,'xtick',[],'xticklabel',[])
set(gca,'ytick',[],'yticklabel',[])
% set(gca,'ztick',[],'zticklabel',[])
axis off
%}


   clc;close all;
   
   %========== data preparing ===============%
   data = importdata('Dataset_48.dat');
   data_size = size(data,1);
   
   xt = zeros(1,16);
   x_cout = 2;
   for i=2:data_size
       if (data(i-1,3) ~= data(i,3))
            xt(x_cout)= i;
            x_cout = x_cout + 1;
       end
   end
   
   xt(end+1) = data_size+1;
   xt_label = 1997:2013;
   
  
   
   %========== single Asset pricing analysis ===============%
   
   % Asset number
   a_n = 4;
   
   
   a_n = 4;
   % ==== daily log price ======== %
   lreturns = diff(log(data(:,a_n)));
   % ==== daily arithmetic price ============ %
   returns = diff(data(:, a_n));
   
   plfit(lreturns(4000:4025))
   h=genhurst2(data(:,4))
   for i=1:100
       [W,t]=bmGenerator(h,2^12,4026); plot(t,W);
       
   %bmGenerator(h);
   hold on;
   end
   xlim([0 4026]);
   
   %figure
   %plot(t,exp(W.^2)-exp(W).^2);
   %size(bmGenerator(h))
   %plfit(returns)
   
   %======== random walk ====== % 
   
   start_point =  data(1,4);
   current_point = start_point;
   
   
   %plot(1,2,'.r');
  
   random_price = zeros(4026,1);
   
   for i=1:1
        for j = 1:4026 
            
            current_point = current_point + randn();
            while (current_point <=0) 
            current_point = current_point + randn();
            end
            
            random_price(j) = current_point;
        end 
        
   end 
   
   
   % =========== processes scaling? ======== %
   for i=1:10
       subplot(2,5,i)
       for q=1:0.02:3
           [a b] = genhurst4(data(:,i+3),q);
           %[a b] = genhurst(random_price,q);
           
       end
       
       
       xlabel('Log(\tau)');
       ylabel('Log(Kq(\tau))');
       set(gca,'xlim',[0,1.2788])
   end
   suptitle('log(Kq(\tau))~qH(q)log(\tau)+log(g(q))')
   %axis auto;
   %axis equal;
  
   
   % ======== uniscaling ? ========== %
   

   result = zeros(1,2);
   for q=0:0.16:4
      [a b] = genhurst2(random_price,q);
      if (q == 0)
        result(size(result,1)+1,:)= [q 0];
      else
        result(size(result,1)+1,:)= [q a*q];
      end
   end
   
   plot(result(:,1),result(:,2),'or-');
   hold on;
   ylim([0 2]);
   xlabel('q');
   ylabel('qH(q)');
   legend('Asset 1');
   p4_pos = get(p4,'position')
   set(p4,'position',[360 278 560 300]);
    
   % ======= calcuate the correcation of the asset and random walk ==== %
   
   % ========= Correlations ======= %
   % two target Asset
   
   
   
   for as_a = 4:13;
   
   
   c = corrcoef(diff(log(data(:,as_a))),diff(log(random_price(:,1))));
   %c = corrcoef(diff(log(data(:,as_a))),diff(log(W(1:4026,:)+data(1,4))));
   maxt =c(1,2);
   
   %ca=figure('numbertitle', 'off', 'name', ... 
    %   ['Corelation of Stock ',num2str(as_a),' and Random Walk']);
   subplot(2,5,as_a-3)
   plot(data(:,as_a));
   hold on;
   plot(random_price(:,1));
   %plot(W(1:4026,:)+data(1,4));
   
   %set(gca,'xticklabel',xt_label,'xtick',xt);
   %ca_pos = get(ca,'position');
   %set(ca,'position', [ca_pos(1:2) 1000 400]); 
   title(['Correlation coeff = ', num2str(maxt)]);
   xlim([0 4100]);
   %lg = legend(['Asset ', num2str(as_a-3)],'Random Walk');
   %set(lg, 'position', [0.91 0.83 0.08 0.06]);
   hold off
   end
    
   % ====== autocorrelation  ============= %
   
   figure('numbertitle', 'off', 'name', ... 
       'Auto correlation of returns');
   [a1, lags1] = autocorr(lreturns, 250);
   plot(lags1, a1, '-r');
   hold on;
   [a2, lags2] = autocorr(abs(lreturns),250);
   plot(lags2, a2, '-g');
   [a3, lags3] = autocorr(lreturns.^2, 250);
   plot(lags3, a3, '-b');
   plot([0 300],[0 0], '-k');
   axis([0 250 -0.1 1]);
   xlabel('lags ');
   ylabel('Autocorrelation');
   legend('log-returns', '|log-returns|','log-returns^2'); 
    
   
   
   
   % ========== different scaling ======= % 
    
%%%%%%%%%%%%%%%%%%%
% stylized fact -6-(seconds)
%%%%%%%%%%%%%%%%%%%
syb = 'v<>^s';
col = 'rbmgc';
k=0;
mm=[1 10 30 60 300];
pr = data(:,4);

figure
for m=mm
    k=k+1;
    lr = log(pr((m+1):m:end))-log(pr(1:m:(end-m)));
    [f,b]=hist(lr,20);
    semilogy(b,f/max(f),['-',syb(k),col(k)])
    hold on
end
axis([-3 +2 1e-4 1.1])
legend(num2str(mm'))
set(gca,'fontsize',14)
xlabel('log-return','fontsize',14)
ylabel('relative freq','fontsize',14)
title('Stock 1')

%
h=genhurst2(pr,2);
k=0;
figure
for m=mm
    k=k+1;
    lr = log(pr((m+1):m:end))-log(pr(1:m:(end-m)));
    [f,b]=hist(lr,20);
    semilogy(b/m.^h,f/max(f),['-',syb(k),col(k)])
    hold on
end
text(0.1005,0.05,['H(2) = ',num2str(h)],'fontsize',14)
%axis([-0.0015 +0.0015 1e-4 1.1])
legend(num2str(mm'))
set(gca,'fontsize',14)
xlabel('log-return','fontsize',14)
ylabel('Relative freq','fontsize',14)
title('Stock 1 ')


returns = diff(log(pr));
%%%%%%%%%%%%%%%%%%%
% stylized fact -2-
%%%%%%%%%%%%%%%%%%%
% figure
% [a1,lags1] = autocorr(returns,150);
% plot(lags1,a1,'-r')
% hold on
% [a2,lags2] = autocorr(abs(returns),150);
% plot(lags2,a2,'-m')
% [a3,lags3] = autocorr(returns.^2,150);
% plot(lags3,a3,'-b')
% plot([0 300],[0 0],'-k')
% axis([0 150 -.25 0.25])
% xlabel('lags (seconds)','fontsize',14)
% ylabel('autocorrelation','fontsize',14)
% title('AAPL autocorrelation of returns','fontsize',14)
% set(gca,'fontsize',14)
% legend({'returns','|returns|','returns^2'})
% print('autocorrelation1sec.eps','-depsc')



   
 
   
 
a = 1;

    % ======= givea normalized pricing firgure ======= %
%    %figure('numbertitle', 'off','name', [' Normalized Assets']);
%    %plot(1:data_size, data(:,4:13)- min(data(:,4:13)))/(max(data(:,4:13)) - min(data(:,4:13)));
%       
%    total = sum(sum(data(:, 4:13)));
%    data1=data; 
%    data2=data;
%    for i=4:13
%        md = mean(data1(:,i));
%        sd = std(data1(:,i));
%        data1(:, i) = data1(:,i)/data1(1,i);
%        data2(:,i)  = (data2(:,i)- md) / sd;
%    end
%    
%    
%    
%    wf = figure('numbertitle','off','name','normalized prices');
%    axes2 = axes('Parent',wf,...
%         'Position',[0.075 0.1 0.80 0.82]);
%    
%    plot(1:data_size,data1(:,4:13),'Parent',axes2);
%    title('normalized prices');
%    
%    wf_leg = legend('Asset 1', 'Asset 2', 'Asset 3', 'Asset 4', ...
%        'Asset 5', 'Asset 6','Asset 7', 'Asset 8', 'Asset 9', 'Asset 10');
%    wf_pos = get(wf,'position');
%    xlim([0 4100]);
%    set(wf,'Position',[wf_pos(1:2) 1000 400]);
%    set(wf_leg,'Position',[0.88 0.13 0.10 0.42]);
%    set(gca,'xticklabel',xt_label,'xtick',xt);
%    
%    
%    
%    
%    
%    wf = figure('numbertitle','off','name','Z-score prices');
%    axes2 = axes('Parent',wf,...
%         'Position',[0.075 0.1 0.80 0.82]);
%    plot(1:data_size,data2(:,4:13),'Parent',axes2);
%    title('z-score prices');
%    
%    hold on;
%    line([1,data_size],[0,0]);
%    
%    wf_leg = legend('Asset 1', 'Asset 2', 'Asset 3', 'Asset 4', ...
%        'Asset 5', 'Asset 6','Asset 7', 'Asset 8', 'Asset 9', 'Asset 10');
%    wf_pos = get(wf,'position');
%    xlim([0 4100]);
%    set(wf,'Position',[wf_pos(1:2) 1000 400]);
%    set(wf_leg,'Position',[0.88 0.13 0.10 0.42]);
%    set(gca,'xticklabel',xt_label,'xtick',xt);
   
    
   %(data(:,4:13) - min(data(:,4:13)))
   %/ (max(data(:,4:13)) - min(data(:,4:13)))
   
 

  
   
   %returns = data(2:end,a_n)-data(1:end,a
   
 
   
   
   
%    f2 = figure('numbertitle','off','name',...
%        ['Asset ',num2str(a_n-3),' returns']);
%    subplot(2,1,1);
%    plot(data(:,a_n));  
%    title(['Asset ',num2str(a_n-3),' price']);
%    %legend(['Asset ',num2str(a_n-3)]);
%    f2_pos = get(f2,'position');
%   xlim([0 4100]);
%    set(f2,'Position',[f2_pos(1:2) 800 600]);
%    set(gca,'xticklabel',xt_label,'xtick',xt);
%    hold on;
   
   
    % =====  ploting Asset price ============ %
  
 f2 = figure('numbertitle','off','name',...
       ['Asset ',num2str(a_n-3),' returns']);
   subplot(2,1,1);
   plot(returns);  
   title(['Asset ',num2str(a_n-3),' simple returns']);
   %legend(['Asset ',num2str(a_n-3)]);
   f2_pos = get(f2,'position');
   xlim([0 4100]);
   set(f2,'Position',[f2_pos(1:2) 800 600]);
   set(gca,'xticklabel',xt_label,'xtick',xt);
   hold on;
   
   
   
   
   % ====== ploting assert logreturn ========%
   
   subplot(2,1,2);
   
   plot(lreturns);
   title(['Asset ',num2str(a_n-3),' log returns']);
   %legend(['Asset ',num2str(a_n-3)]);
   xlim([0 4100]);
   set(gca,'xticklabel',xt_label,'xtick',xt);
   
   
   
   


