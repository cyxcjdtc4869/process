
   clc;close all;
   
   %========== data preparing ===============%
   data = importdata('Dataset_48.dat');
   data_size = size(data,1);
   
   xt = zeros(1,16);
   x_cout = 2;
   for i=2:data_size
       if (data(i-1,3) ~= data(i,3))
            xt(x_cout)= i;
            x_cout = x_cout + 1;
       end
   end
   
   xt(end+1) = data_size+1;
   xt_label = 1997:2013;
   
  
   
   %========== single Asset pricing analysis ===============%
   
   % Asset number
   a_n = 4;
   
   
   a_n = 4;
   % ==== daily log price ======== %
   lreturns = diff(log(data(:,a_n)));
   % ==== daily arithmetic price ============ %
   returns = diff(data(:, a_n));
   
   plfit(lreturns(4000:4025))
   h=genhurst2(data(:,4))
   for i=1:100
       [W,t]=bmGenerator(h,2^12,4026); plot(t,W);
       
   %bmGenerator(h);
   hold on;
   end
   xlim([0 4026]);
   
   %figure
   %plot(t,exp(W.^2)-exp(W).^2);
   %size(bmGenerator(h))
   %plfit(returns)
   
   %======== random walk ====== % 
   
   start_point =  data(1,4);
   current_point = start_point;
   
   
   %plot(1,2,'.r');
  
   random_price = zeros(4026,1);
   
   for i=1:1
        for j = 1:4026 
            
            current_point = current_point + randn();
            while (current_point <=0) 
            current_point = current_point + randn();
            end
            
            random_price(j) = current_point;
        end 
        
   end 
   
   
   % =========== processes scaling? ======== %
   for i=1:10
       subplot(2,5,i)
       for q=1:0.02:3
           [a b] = genhurst4(data(:,i+3),q);
           %[a b] = genhurst(random_price,q);
           
       end
       
       
       xlabel('Log(\tau)');
       ylabel('Log(Kq(\tau))');
       set(gca,'xlim',[0,1.2788])
   end
   suptitle('log(Kq(\tau))~qH(q)log(\tau)+log(g(q))')
   %axis auto;
   %axis equal;
  
   
   % ======== uniscaling ? ========== %
   

   result = zeros(1,2);
   for q=0:0.16:4
      [a b] = genhurst2(random_price,q);
      if (q == 0)
        result(size(result,1)+1,:)= [q 0];
      else
        result(size(result,1)+1,:)= [q a*q];
      end
   end
   
   plot(result(:,1),result(:,2),'or-');
   hold on;
   ylim([0 2]);
   xlabel('q');
   ylabel('qH(q)');
   legend('Asset 1');
   p4_pos = get(p4,'position')
   set(p4,'position',[360 278 560 300]);
    
   % ======= calcuate the correcation of the asset and random walk ==== %
   
   % ========= Correlations ======= %
   % two target Asset
   
   
   
   for as_a = 4:13;
   
   
   c = corrcoef(diff(log(data(:,as_a))),diff(log(random_price(:,1))));
   %c = corrcoef(diff(log(data(:,as_a))),diff(log(W(1:4026,:)+data(1,4))));
   maxt =c(1,2);
   
   %ca=figure('numbertitle', 'off', 'name', ... 
    %   ['Corelation of Stock ',num2str(as_a),' and Random Walk']);
   subplot(2,5,as_a-3)
   plot(data(:,as_a));
   hold on;
   plot(random_price(:,1));
   %plot(W(1:4026,:)+data(1,4));
   
   %set(gca,'xticklabel',xt_label,'xtick',xt);
   %ca_pos = get(ca,'position');
   %set(ca,'position', [ca_pos(1:2) 1000 400]); 
   title(['Correlation coeff = ', num2str(maxt)]);
   xlim([0 4100]);
   %lg = legend(['Asset ', num2str(as_a-3)],'Random Walk');
   %set(lg, 'position', [0.91 0.83 0.08 0.06]);
   hold off
   end
    
   % ====== autocorrelation  ============= %
   
   figure('numbertitle', 'off', 'name', ... 
       'Auto correlation of returns');
   [a1, lags1] = autocorr(lreturns, 250);
   plot(lags1, a1, '-r');
   hold on;
   [a2, lags2] = autocorr(abs(lreturns),250);
   plot(lags2, a2, '-g');
   [a3, lags3] = autocorr(lreturns.^2, 250);
   plot(lags3, a3, '-b');
   plot([0 300],[0 0], '-k');
   axis([0 250 -0.1 1]);
   xlabel('lags ');
   ylabel('Autocorrelation');
   legend('log-returns', '|log-returns|','log-returns^2'); 
    
   
   
   
   % ========== different scaling ======= % 
    
%%%%%%%%%%%%%%%%%%%
% stylized fact -6-(seconds)
%%%%%%%%%%%%%%%%%%%
syb = 'v<>^s';
col = 'rbmgc';
k=0;
mm=[1 10 30 60 300];
pr = data(:,4);

figure
for m=mm
    k=k+1;
    lr = log(pr((m+1):m:end))-log(pr(1:m:(end-m)));
    [f,b]=hist(lr,20);
    semilogy(b,f/max(f),['-',syb(k),col(k)])
    hold on
end
axis([-3 +2 1e-4 1.1])
legend(num2str(mm'))
set(gca,'fontsize',14)
xlabel('log-return','fontsize',14)
ylabel('relative freq','fontsize',14)
title('Stock 1')

%
h=genhurst2(pr,2);
k=0;
figure
for m=mm
    k=k+1;
    lr = log(pr((m+1):m:end))-log(pr(1:m:(end-m)));
    [f,b]=hist(lr,20);
    semilogy(b/m.^h,f/max(f),['-',syb(k),col(k)])
    hold on
end
text(0.1005,0.05,['H(2) = ',num2str(h)],'fontsize',14)
%axis([-0.0015 +0.0015 1e-4 1.1])
legend(num2str(mm'))
set(gca,'fontsize',14)
xlabel('log-return','fontsize',14)
ylabel('Relative freq','fontsize',14)
title('Stock 1 ')


returns = diff(log(pr));
%%%%%%%%%%%%%%%%%%%
% stylized fact -2-
%%%%%%%%%%%%%%%%%%%
% figure
% [a1,lags1] = autocorr(returns,150);
% plot(lags1,a1,'-r')
% hold on
% [a2,lags2] = autocorr(abs(returns),150);
% plot(lags2,a2,'-m')
% [a3,lags3] = autocorr(returns.^2,150);
% plot(lags3,a3,'-b')
% plot([0 300],[0 0],'-k')
% axis([0 150 -.25 0.25])
% xlabel('lags (seconds)','fontsize',14)
% ylabel('autocorrelation','fontsize',14)
% title('AAPL autocorrelation of returns','fontsize',14)
% set(gca,'fontsize',14)
% legend({'returns','|returns|','returns^2'})
% print('autocorrelation1sec.eps','-depsc')



   
 
   
 
a = 1;

    % ======= givea normalized pricing firgure ======= %
%    %figure('numbertitle', 'off','name', [' Normalized Assets']);
%    %plot(1:data_size, data(:,4:13)- min(data(:,4:13)))/(max(data(:,4:13)) - min(data(:,4:13)));
%       
%    total = sum(sum(data(:, 4:13)));
%    data1=data; 
%    data2=data;
%    for i=4:13
%        md = mean(data1(:,i));
%        sd = std(data1(:,i));
%        data1(:, i) = data1(:,i)/data1(1,i);
%        data2(:,i)  = (data2(:,i)- md) / sd;
%    end
%    
%    
%    
%    wf = figure('numbertitle','off','name','normalized prices');
%    axes2 = axes('Parent',wf,...
%         'Position',[0.075 0.1 0.80 0.82]);
%    
%    plot(1:data_size,data1(:,4:13),'Parent',axes2);
%    title('normalized prices');
%    
%    wf_leg = legend('Asset 1', 'Asset 2', 'Asset 3', 'Asset 4', ...
%        'Asset 5', 'Asset 6','Asset 7', 'Asset 8', 'Asset 9', 'Asset 10');
%    wf_pos = get(wf,'position');
%    xlim([0 4100]);
%    set(wf,'Position',[wf_pos(1:2) 1000 400]);
%    set(wf_leg,'Position',[0.88 0.13 0.10 0.42]);
%    set(gca,'xticklabel',xt_label,'xtick',xt);
%    
%    
%    
%    
%    
%    wf = figure('numbertitle','off','name','Z-score prices');
%    axes2 = axes('Parent',wf,...
%         'Position',[0.075 0.1 0.80 0.82]);
%    plot(1:data_size,data2(:,4:13),'Parent',axes2);
%    title('z-score prices');
%    
%    hold on;
%    line([1,data_size],[0,0]);
%    
%    wf_leg = legend('Asset 1', 'Asset 2', 'Asset 3', 'Asset 4', ...
%        'Asset 5', 'Asset 6','Asset 7', 'Asset 8', 'Asset 9', 'Asset 10');
%    wf_pos = get(wf,'position');
%    xlim([0 4100]);
%    set(wf,'Position',[wf_pos(1:2) 1000 400]);
%    set(wf_leg,'Position',[0.88 0.13 0.10 0.42]);
%    set(gca,'xticklabel',xt_label,'xtick',xt);
   
    
   %(data(:,4:13) - min(data(:,4:13)))
   %/ (max(data(:,4:13)) - min(data(:,4:13)))
   
 

  
   
   %returns = data(2:end,a_n)-data(1:end,a
   
 
   
   
   
%    f2 = figure('numbertitle','off','name',...
%        ['Asset ',num2str(a_n-3),' returns']);
%    subplot(2,1,1);
%    plot(data(:,a_n));  
%    title(['Asset ',num2str(a_n-3),' price']);
%    %legend(['Asset ',num2str(a_n-3)]);
%    f2_pos = get(f2,'position');
%   xlim([0 4100]);
%    set(f2,'Position',[f2_pos(1:2) 800 600]);
%    set(gca,'xticklabel',xt_label,'xtick',xt);
%    hold on;
   
   
    % =====  ploting Asset price ============ %
  
 f2 = figure('numbertitle','off','name',...
       ['Asset ',num2str(a_n-3),' returns']);
   subplot(2,1,1);
   plot(returns);  
   title(['Asset ',num2str(a_n-3),' simple returns']);
   %legend(['Asset ',num2str(a_n-3)]);
   f2_pos = get(f2,'position');
   xlim([0 4100]);
   set(f2,'Position',[f2_pos(1:2) 800 600]);
   set(gca,'xticklabel',xt_label,'xtick',xt);
   hold on;
   
   
   
   
   % ====== ploting assert logreturn ========%
   
   subplot(2,1,2);
   
   plot(lreturns);
   title(['Asset ',num2str(a_n-3),' log returns']);
   %legend(['Asset ',num2str(a_n-3)]);
   xlim([0 4100]);
   set(gca,'xticklabel',xt_label,'xtick',xt);
   
   
   
   
