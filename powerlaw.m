Portreturn = csvread('logrv.csv');
Port_mean = mean(Portreturn);
Port_std = std(Portreturn);
Port_skewness = skewness(Portreturn);
Port_kurtosis = kurtosis(Portreturn);
figure;
loglog(sort( Portreturn(Portreturn>0)),1-[1:(length(Portreturn(Portreturn>0)))]/length(Portreturn(Portreturn>0)),'.b')
hold on
loglog(sort(-Portreturn(Portreturn<0)),1-[1:(length(Portreturn(Portreturn<0)))]/length(Portreturn(Portreturn<0)),'.r')
xx = 1e-5:0.01:1;
loglog(ones(length(xx))*(Port_mean+Port_std*1), xx,'c',...
    ones(length(xx))*(Port_mean+Port_std*2), xx,'c',...
    ones(length(xx))*(Port_mean+Port_std*3), xx,'c',...
    ones(length(xx))*(Port_mean+Port_std*10),xx,'c')
    % compare with normal distribution
    x = [max(Portreturn)/1000:max(Portreturn)/1000:max(Portreturn)];
    loglog(x,1-(normcdf(x,Port_mean,Port_std)-0.5)*2,'-m','linewidth',2)
    [alpha, xmin]=plfit(Portreturn,'nosmall');
    n = length(Portreturn);
    c = [sort(abs(Portreturn)) (n:-1:1)'./n];
    q = sort(Portreturn(abs(Portreturn)>=xmin));
    cf = [q (q./xmin).^(1-alpha)];
    cf(:,2) = cf(:,2) .* c(find(c(:,1)>=xmin,1,'first'),2);
    loglog(cf(:,1),cf(:,2),'k--','LineWidth',2);
    set(gca,'ylim',[10e-5,1]);
    legend({'pos ret','neg ret'})
    title('Complemetary cumulative log-return distribution & Power Law','fontsize',10)
    xlabel('log-return','fontsize',10)
    ylabel('complemetary cumulative distribution','fontsize',10)
    set(gca,'fontsize',10)